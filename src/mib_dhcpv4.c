/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_dhcpv4.h"

#define ME "dhcpv4_mib"
#define MOD "dhcpv4"
#define string_empty(x) ((x == NULL) || (*x == '\0'))

static void update_flag(const char* netmodel_intf, const char* flag, bool set) {
    if(set) {
        netmodel_setFlag(netmodel_intf, flag, "", netmodel_traverse_this);
    } else {
        netmodel_clearFlag(netmodel_intf, flag, "", netmodel_traverse_this);
    }
}

static bool is_dhcp_bound(const char* status) {
    bool ret = false;

    when_str_empty(status, exit);

    if((strcmp(status, "Bound") == 0) ||
       (strcmp(status, "Renewing") == 0) ||
       (strcmp(status, "Rebinding") == 0)) {
        ret = true;
    }

exit:
    return ret;
}

static amxs_status_t dhcp_status_action_cb(const amxs_sync_entry_t* sync_entry,
                                           amxs_sync_direction_t direction,
                                           amxc_var_t* data,
                                           void* priv) {
    amxs_status_t rv = amxs_status_unknown_error;
    const char* netmodel_intf = GET_CHAR(data, "path");
    const char* dhcp_status = GETP_CHAR(data, "parameters.DHCPStatus");
    bool bound = is_dhcp_bound(dhcp_status);

    // Execute standard copy action
    rv = amxs_sync_param_copy_action_cb(sync_entry, direction, data, priv);
    when_false(rv == amxs_status_ok, exit);

    when_false((direction == amxs_sync_a_to_b), exit);

    update_flag(netmodel_intf, "dhcpv4-bound", bound);

exit:
    return rv;
}

static amxs_status_t status_action_cb(const amxs_sync_entry_t* sync_entry,
                                      amxs_sync_direction_t direction,
                                      amxc_var_t* data,
                                      void* priv) {
    amxs_status_t rv = amxs_status_unknown_error;
    const char* netmodel_intf = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.DHCPv4Status");
    bool is_up = !string_empty(status) && (strcmp(status, "Enabled") == 0);

    // Execute standard copy action
    rv = amxs_sync_param_copy_action_cb(sync_entry, direction, data, priv);
    when_false(rv == amxs_status_ok, exit);

    when_false((direction == amxs_sync_a_to_b), exit);

    update_flag(netmodel_intf, "dhcpv4-up", is_up);

exit:
    return rv;
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_unknown_error;
    amxs_sync_object_t* obj = NULL;

    status = amxs_sync_ctx_add_new_param(ctx, "DHCPStatus", "DHCPStatus", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, dhcp_status_action_cb, ctx);
    status |= amxs_sync_ctx_add_new_param(ctx, "Status", "DHCPv4Status", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, status_action_cb, ctx);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "IPAddress", "IPAddress", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "IPRouters", "IPRouters", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "DNSServers", "DNSServers", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "ResetOnPhysDownTimeout", "ResetOnPhysDownTimeout", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_new_copy(&obj, "ReqOption.", "ReqOption.", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Alias", "Alias", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Enable", "Enable", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Tag", "Tag", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Value", "Value", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_object(ctx, obj);

    return status;
}

static char* find_instance(amxb_bus_ctx_t* bus, const char* interface_path) {
    char* instance = NULL;
    amxc_var_t ret;
    amxc_string_t search_path;
    const char* key = NULL;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_string_init(&search_path, 0);

    if(string_empty(interface_path)) {
        SAH_TRACEZ_ERROR(ME, "Missing interface path");
        goto exit;
    }

    amxc_string_setf(&search_path, "DHCPv4Client.Client.[Interface == \"%s\"].", interface_path);
    rv = amxb_get(bus, amxc_string_get(&search_path, 0), 0, &ret, 5);
    if(AMXB_STATUS_OK != rv) {
        SAH_TRACEZ_ERROR(ME, "Failed to get instance %s, reason: %d", amxc_string_get(&search_path, 0), rv);
        goto exit;
    }

    key = amxc_var_key(GETP_ARG(&ret, "0.0"));
    if(string_empty(key)) {
        SAH_TRACEZ_ERROR(ME, "Instance %s not found", amxc_string_get(&search_path, 0));
        goto exit;
    }

    instance = strdup(key);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path);
    return instance;
}

static netmodel_mib_t mib = {
    .name = ME,
    .root = "DHCPv4Client.",
    .flags = NETMODEL_MIB_DONT_LINK_INTERFACES |
        NETMODEL_MIB_ONLY_MY_PARAMETERS,
    .add_sync_parameters = add_parameters,
    .find_instance_to_sync = find_instance,
};

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_subscribe(args, &mib);

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_unsubscribe(args, &mib);

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    netmodel_initialize();

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    netmodel_cleanup();

    return 0;
}
