# netmodel-dhcp4

This netmodel client handles the synchronization between netmodel interfaces marked with the dhcpv4 flag and the DHCPv4.Client.Intf. instances.