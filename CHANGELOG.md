# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.3.1 - 2024-11-13(18:52:45 +0000)

### Other

- [Mib-dhcpv4] Renew is triggering a netmodel dhcpv4-bound

## Release v1.3.0 - 2023-10-03(06:57:37 +0000)

### New

- [amx][conmon] ConMon must support arp(ipv4) and icmpv6(ipv6)

## Release v1.2.0 - 2023-09-26(11:39:43 +0000)

### New

- [AP Config][IPv4 connectivity] Repeater should switch from static to dhcp ip address

## Release v1.1.0 - 2023-03-13(15:53:27 +0000)

### Fixes

- [DHCPv4] Split client and server plugin

## Release v1.0.1 - 2022-04-14(07:26:44 +0000)

### Changes

- Make the Interface path prefixed

## Release v1.0.0 - 2022-02-28(10:58:55 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.2.2 - 2022-01-27(13:05:11 +0000)

### Fixes

- wrong mib can be unsubscribed when netmodel clients share a netmodel interface

## Release v0.2.1 - 2022-01-21(16:26:11 +0000)

### Changes

- expand libnetmodel for mibs/netmodel-clients

## Release v0.2.0 - 2022-01-11(08:27:50 +0000)

### New

- Create DHCPv4 Client mapping in NetModel

## Release v0.1.0 - 2022-01-06(09:03:27 +0000)

### New

- Create DHCPv4 Client mapping in NetModel

